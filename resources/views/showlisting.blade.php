@extends('layouts.app')
@include('components.header')
@include('components.sidebar')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{$listing->name}} <a href="/" class="float-right btn btn-default btn-sx">Go Back</a></div>

                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item">Address: {{$listing->address}}</li>
                        <li class="list-group-item"><a href="{{$listing->website}}" target= "_blank">{{$listing->website}}</a></li>
                        <li class="list-group-item">Phone: {{$listing->phone}}</li>
                        <li class="list-group-item">Email: {{$listing->email}}</li>
                    </ul>
                    <div class="Jumbotron">
                        {{$listing->bio}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

