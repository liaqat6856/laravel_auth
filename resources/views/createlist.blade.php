@extends('layouts.app')

@include('components.header')
@include('components.sidebar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard <span class="float-right"><a href="/dashboard" class="btn btn-default">Back</a></span></div>

                <div class="card-body">
                    <h3>Your Listing</h3>
                    <form action="{{ route('listings.post')}}" method="POST"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">User Id</label>
                            <input type="text" name="user_id" class="form-control">
                            @error('name')
                                    <span class="alert alert-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control">
                            @error('name')
                                    <span class="alert alert-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" name="email"  class="form-control">
                            @error('email')
                                    <span class="alert alert-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Phone No.</label>
                            <input type="number" name="phone"  class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Address</label>
                            <input type="text" name="adress"  class="form-control">
                        </div>
                        <div class="form-group">
                            <label for=""> Website</label>
                            <input type="text" name="website"  class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Bio</label>
                           <textarea  id="" cols="30" rows="10" class="form-control"  name="bio"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Create List</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
