@extends('layouts.app')

@include('components.header')
@include('components.sidebar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">File Upload </div>

                <div class="card-body">
                    <h3>Your Listing</h3>
                    <form action="upload" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control">
                            @error('name')
                                    <span class="alert alert-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Secton</label>
                            <input type="text" name="section"  class="form-control">
                            @error('email')
                                    <span class="alert alert-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Roll No.</label>
                            <input type="number" name="rollN"  class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Address</label>
                            <input type="text" name="address"  class="form-control">
                        </div>
                        <div class="form-group">
                            <label for=""> Phone</label>
                            <input type="number" name="phone"  class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="file" name="file"  class="form-control">
                        </div>
                       
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Upload File</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
