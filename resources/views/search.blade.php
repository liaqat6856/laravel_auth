@extends('layouts.app')

@include('components.navbar')
@include('components.sidebar')

@section('content')
  <form action="" method="post" class="form-inline">
      @csrf
      <input type="text" name="search" placeholder="Search Grocery" class="form-control" style="width: 300px;">
      <input type="submit" value="submit" class="btn btn-default">
  </form>
    <div class="container">
    <h1>Grocery Lists</h1>
    <table class="table table-strict">
        <tr>
            <th>name</th>
            <th>type</th>
            <th>price</th>
        </tr>
        @foreach($lists as $list)
            <tr>
                <td>{{ $list['name']}}</td>
                <td>{{ $list['type']}}</td>
                <td>{{ $list['price']}}</td>
            </tr>
        @endforeach
    </table>
    
    </div> 
    <div class="container" style="display:flex;justify-content: center;padding-top: 50px;">{{$lists->links()}}</div>
@endsection