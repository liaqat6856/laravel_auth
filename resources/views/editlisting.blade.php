@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard <span class="float-right"><a href="/dashboard" class="btn btn-default">Back</a></span></div>

                <div class="card-body">
                    <h3>Edit Listing</h3>
                    <form action="{{route('listings.update', $listings->id )}}" method="post">
                        
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Name</label>
                            <input name="name" type="text" value="{{$listings->name}}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input name="email" type="email" value="{{$listings->email}}"  class="form-control">
                        </div>
                        <div class="form-group">
                            <label for=""> Website</label>
                            <input name="website" type="text" value="{{$listings->website}}"  class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Phone No.</label>
                            <input name="phone" type="number" value="{{$listings->phone}}"  class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Address</label>
                            <input name="adress" type="text" value="{{$listings->address}}"  class="form-control">
                        </div>
                        
                            
                           
                        
                        <div class="form-group">
                            <label for="">Bio</label>
                           <textarea name="bio"  id="" cols="30" rows="10" class="form-control" >{{$listings->bio}}</textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Edit List</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
