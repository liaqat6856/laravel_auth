<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/dist/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/assets/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="{{ url('/')}}/assets/dist/css/adminlte.min.css">
</head>
<body>
    <div class="wrapper">
         
              <div class="content-wrapper" style="min-height: 400px;">
                @yield('content')
              </div>
              @include('components.footer')
    </div>
    <script src="{{ url('/')}}/assets/plugins/jquery/jquery.min.js"></script>
    <script src="{{ url('/')}}/assets/dist/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{{ url('/')}}/assets/dist/js/adminlte.min.js"></script>


</body>
</html>
