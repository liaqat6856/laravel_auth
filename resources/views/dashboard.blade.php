@extends('layouts.app')
@include('components.header')
@include('components.sidebar')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard  <span class="float-right"><a href="{{route('listings.create')}}" class="btn btn-success"> Create List</a></span></div>

                <div class="card-body">
                    <h3>Your Listing</h3>
                    
                    @if (count($listings))
                        <table class="table table-striped">
                            <tr>
                                <th>Company</th>
                                <th></th>
                                <th></th>
                            </tr>
                            @foreach($listings as $listing)
                               <tr>
                                   <td>{{ $listing->name }}</td>
                                   <td class="pull-right"><a class=" btn btn-default" href="{{ url('/listings/edit/'.$listing->id) }}" style="float: right;">Edit</a></td>
                                   <td class="pull-right"><a class=" btn btn-danger" href="{{ url('/listings/delete/'.$listing->id) }}">Delete</a></td>
                               </tr>
                            @endforeach
                        </table>
                    @endif
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

