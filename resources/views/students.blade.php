@extends('layouts.app')

@include('components.header')
@include('components.sidebar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard <span class="float-right"><a href="/dashboard" class="btn btn-default">Back</a></span></div>

                <div class="card-body">
                    <h3>Student Data</h3>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
