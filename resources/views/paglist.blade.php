@extends('layouts.app')

@include('components.header')
@include('components.sidebar')

@section('content')
<div class="container">
    <h1>This is Pagination Page</h1>
    <table class="table table-Striped">
    <tr>
            <th>user_id</th>
            <th>name</th>
            <th>email</th>
            <th>website</th>
            <th>address</th>
            <th>phone</th>
            <th>bio</th>
            <th>file</th>
        </tr>
    @foreach($members as $member)
        <tr>
            <td>{{$member['user_id']}}</td>
            <td>{{$member['name']}}</td>
            <td>{{$member['email']}}</td>
            <td>{{$member['website']}}</td>
            <td>{{$member['address']}}</td>
            <td>{{$member['phone']}}</td>
            <td>{{$member['bio']}}</td>
            <td>{{$member['file']}}</td>

        </tr>
    @endforeach
    </table>
 </div>
 <div class="container text-center" >{{ $members->links() }}</div>
@endsection