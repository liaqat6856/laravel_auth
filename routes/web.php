<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ListingsController@index');
Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/listings/show/{id}', 'ListingsController@show')->name('listings.show');
Route::get('/listings/create', 'ListingsController@create')->name('listings.create');
Route::post('/listings/store', 'ListingsController@store')->name('listings.post');
Route::get('/listings/edit/{id}', 'ListingsController@edit')->name('listings.edit');
Route::post('/listings/update/{id}', 'ListingsController@update')->name('listings.update');
Route::get('/listings/delete/{id}', 'ListingsController@destroy')->name('listing.delete');
// Route::resource('listing', 'ListingsController');
Route::get('/student', 'StudentController@stuData');
Route::view('/fileuplode', 'fileuplode');
Route::view('/students', 'students');
Route::post('/upload', 'UploadController@uploadData');

Route::view('/grocery', 'grocery');
Route::post('/groceryp', 'GroceryController@index');
Route::view('/grocerylist', 'grocerylist');
Route::view('pagination', 'pagination');
Route::get('paglist', 'PaginationController@pagination');
Route::get('/search', 'GroceryListContrller@search');
Route::get('/search', 'GroceryListContrller@index');