<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Students;
class UploadController extends Controller
{
    //
    function uploadData(Request $request) {
        $request->validate([
            'file' => 'required|mimes:png,jpg,jpeg,csv,txt,xlx,xls,pdf|max:2048'
            ]);
        
            $student = new Students;
            $student->name = $request->input('name');
            $student->section = $request->input('section');
            $student->rollN = $request->input('rollN');
            $student->address = $request->input('address');
            $student->phone = $request->input('phone');
            $student->file = $request->input('file');
            
            
            if($request->file()) {
                $name = time().'_'.$request->student; //->getClientOriginalName()
                $filePath = $request->file('file')->storeAs('uploads', $name, 'public');
    
                
                $student->file = '/storage/' . $filePath;
                $student->save();
    
                return back()
                ->with('success','File has uploaded to the database.')
                ->with('file', $name);
                
            }
            $student->save();

            return redirect('/students')->with('success', 'student Aded');

    }
}
