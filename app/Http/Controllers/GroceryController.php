<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grocery;

class GroceryController extends Controller
{
    //

    public function index(Request $request)
 {      
        $request->validate ([
            "name" => "required",
            "type" => "required",
            "price" => "required"
        ]);
        
        $grocery = new Grocery;

        $grocery->name = $request->input('name');
        $grocery->type = $request->input('type');
        $grocery->price = $request->input('price');
         
        $grocery->save();
       
       return redirect('/grocerylist')->with('grocerys', $grocery);
 }
 
}
