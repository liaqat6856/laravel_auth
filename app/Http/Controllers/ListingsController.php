<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\listing;

class ListingsController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    } 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listings = listing::orderBy('created_at', 'desc')->get();

        return view('listings')->with('listings', $listings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('createlist');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate ([
            "name" => "required",
            "email" => "email",
            "password" => "password"
        ]);

        // $data = $request->all();

        // $new_media = listing::create($data);
       
       //Create List
       $user_id = auth()->user()->id;
       $listing = new listing;
       $listing->user_id = $user_id;
       $listing->name = $request->input('name');
       $listing->email = $request->input('email');
       $listing->phone = $request->input('phone');
       $listing->address = $request->input('adress');
       $listing->website = $request->input('website');
       $listing->bio = $request->input('bio');
       
    
   
       $listing->save();

       return redirect('/dashboard')->with('success', 'Listing Aded');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $listings =listing::find($id);
       
        // dd($single_list->name );
        return view('showlisting')->with( 'listing', $listings);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $single_list =listing::find($id);
        // dd($single_list->name );
        return view('editlisting')->with( 'listings', $single_list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate ([
            "name" => "required",
            "email" => "email",
            "password" => "password"
        ]);
        // $data = $request->all();

        //Create List
       $user_id = auth()->user()->id;
       $listing =  listing::find($id);
       $listing->user_id = $user_id;
       $listing->name = $request->input('name');
       $listing->email = $request->input('email');
       $listing->website = $request->input('website');
       $listing->phone = $request->input('phone');
       $listing->address = $request->input('adress');
       $listing->bio = $request->input('bio');
   
       $listing->save();

       return redirect('/dashboard')->with('success', 'Listing Editted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $listing =  listing::find($id);

        $listing->delete();

        return redirect('/dashboard')->with('success', 'Listing Deleted');
    }
}
