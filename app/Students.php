<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    // protected $fillable = [ "name", "section", "rollN", "address", "phone"];
    public function user () {
        return $this->belongsTo('App\User');
    }
}
